import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isAuth: false,
  isVerified: false,
  isAddedDetails: false,
  name: null,
  roll: null,
  email: null,
  course: null,
  semester: null,
  section: null,
};

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setUserData(state, action) {
      state.isAuth = true;
      Object.entries(action.payload).forEach(
        ([key, value]) => (state[key] = value)
      );
    },

    logOut(state) {
      Object.entries(state).forEach(([key]) => (state[key] = null));
    },
  },
});

export const userActions = userSlice.actions;
export default userSlice.reducer;
