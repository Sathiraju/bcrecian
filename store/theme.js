import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  color: "light",
};

const themeSlice = createSlice({
  name: "theme",
  initialState,
  reducers: {
    toggleTheme(state) {
      const body = document.getElementsByTagName("body")[0];
      if (state.color === "light") {
        body.classList.add("dark");
        state.color = "dark";
      } else {
        body.classList = "";
        body.classList.add("light");
        state.color = "light";
      }
      localStorage.setItem("theme", state.color);
    },
    setTheme(state, action) {
      const body = document.getElementsByTagName("body")[0];
      body.classList = "";
      body.classList.add(action.payload);
      state.color = action.payload;
    },
  },
});

export const themeActions = themeSlice.actions;
export default themeSlice.reducer;
