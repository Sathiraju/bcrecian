import { CgNotes } from "react-icons/cg";
import Notes from "../../components/Grid/Notes/Index";
import Section from "../../components/Section/Section";
import styles from "./index.module.scss";

const index = () => {
  return (
    <div className={styles.notes}>
      <Section title="Notes" Icon={CgNotes} Component={Notes} />
    </div>
  );
};

export default index;
