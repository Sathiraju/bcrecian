require('../DB/conn');
const Semester = require("../models/semesterSchema")

export default async function handler(req, res) {
    try {
        let semester = await Semester.find({});
        if (semester) {
            return res.status(200).json({ semesters: semester })
        }
        else {
            return res.status(422).json({ error: "No Semester added!" });
        }
    } catch (e) {
        console.log(e)
        return res.status(500).json({ error: "Internal Server Error" });
    }

}