const mongoose = require("mongoose");
const courses = new mongoose.Schema({
    courseName: {
        type: String,
        required: true
    },
    semesters: {
        type: Array
    }
});


let course;
try {
    course = mongoose.model('COURSE')
} catch (error) {
    course = mongoose.model('COURSE', courses);
}
module.exports = course;