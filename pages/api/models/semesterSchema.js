const mongoose = require("mongoose");
const semesters = new mongoose.Schema({
    semesterName: {
        type: String,
        required: true
    }
});


let semester;
try {
    semester = mongoose.model('SEMESTER')
} catch (error) {
    semester = mongoose.model('SEMESTER', semesters);
}
module.exports = semester;