const mongoose = require("mongoose");
const otps = new mongoose.Schema({
    _userId: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'user' },
    otp: { type: String, required: true },
    expireAt: { type: Date, default: function () { return new Date(Date.now() + 1000 * 24 * 3600); } }
},
    {
        collection: "otps",
        timestamps: true
    }
);

// otps.path("createdAt").index({ expires: 500 * 24 * 3600 });
let otp;
try {
    otp = mongoose.model('OTP')
} catch (error) {
    otp = mongoose.model('OTP', otps);
}
module.exports = otp;