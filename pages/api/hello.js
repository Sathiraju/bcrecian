// API route : localhost:3000/api/hello ( try it !)
const jwt = require("jsonwebtoken");
const Cookies = require("cookies");
const User = require("./models/userSchema");
export default async function handler(req, res) {
  try {
    const cookies = new Cookies(req, res);
    // Get a cookie
    const token = cookies.get("jwt");
    const verifyUser = await jwt.verify(token, "mynameisaanandagopaldutta");
    console.log(verifyUser);
    const userExist = await User.findOne({ _id: verifyUser._id });
    res.status(200).json({ name: "hii " + userExist.name });
  } catch (err) {
    res.status(422).json({ error: "please login first" });
  }
}
