const nodemailer = require("nodemailer");
const hbs = require("nodemailer-express-handlebars");
const fs = require("fs");
const path = require("path");
const env = require("../config");

const sendMail = async function (otp, receiver, name, emailBody, button_visibility = "block") {
    var transporter = nodemailer.createTransport({
        service: "gmail",
        host: 'smtp.gmail.com',
        port: 465,
        secure: true,
        auth: {
            user: env.mpEmail,
            pass: env.mpPassword,
        },
        from: env.mpEmail,
    });

    let __username__ = name.split(" ")[0];
    let __userverificationlink__ = otp;
    let __head__ = emailBody.head;
    let __body__ = emailBody.body;
    var mailOptions = {
        from: env.mpEmail,
        to: receiver,
        subject: "Confirmation Mail",
        html: `
            <body style="color:#333333">
                <div class="container" style="color:#333333">
                <div class="header" style="background-color: #0061c3;color: #fff;text-align:center;padding:2rem;">
                    <h1>Welcome</h1>
                    <P>${__head__}</P>
                </div>
                <div class="main_body" style="color:#333333">
                    <p class="name" style="font-size: 20px; font-weight:500; color:#333333;">Hello ${__username__}!</p>
                    <p class="disc" style="color:#333333">${__body__}<span style="color: #0061c3;">minorproj2021@gmail.com</span>.</p>
                </div>
                <div class="button" style="text-align: center; padding:2rem; display:${button_visibility}">
                    <a href="${__userverificationlink__}"
                    style="background-color:#0061c3; padding:1rem 1.5rem;color:#fff;text-decoration:none;border-radius:5px">Click
                    Here</a>
                </div>
                <hr width="80%" style="margin-top: 2rem;">
                </div>

                <div class="footer" style="text-align: center; color:#333333;">
                    <p>&copy;College Space 2021 . All Rights Reserved.</p>
                </div>
                </div>
            </body>`,
    };

    let error = await transporter.sendMail(mailOptions);
    if (!error) {
        console.log(error);
    } else {
        console.log("Email sent to " + receiver);
    }
};

module.exports = sendMail;
