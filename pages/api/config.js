const env = {
  mpDB: process.env.mpDB,
  mpEmail: process.env.mpEmail,
  mpPassword: process.env.mpPassword,
  mpJWTSecretKey: process.env.mpJWTSecretKey,
};

// console.log(env); // For Testing Purpose.
module.exports = env;
