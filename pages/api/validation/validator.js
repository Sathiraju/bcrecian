const emailValidation = function (email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

const nameValidation = function (name) {
    const re = /^[a-zA-Z ]{2,30}$/;
    return re.test(String(name));
}
const rollValidation = function (roll) {
    const re = /^[1-9]\d{10,10}$/;
    return re.test(roll);
}
const passwordValidation = function (password) {
    const re = /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{8,})/;
    return re.test(password);
}
const cPasswordCheck = function (password, cPassword) {
    let isTrue = (password == cPassword) ? true : false;
    return isTrue;
}
module.exports = { emailValidation, nameValidation, rollValidation, passwordValidation, cPasswordCheck };
