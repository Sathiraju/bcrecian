require('../DB/conn');
const Section = require("../models/sectionSchema")

export default async function handler(req, res) {
    try {
        let sections = await Section.find({});
        if (sections) {
            return res.status(200).json({ courses: sections })
        }
        else {
            return res.status(422).json({ error: "No Section added!" });
        }
    } catch (e) {
        console.log(e)
        return res.status(500).json({ error: "Internal Server Error" });
    }

}