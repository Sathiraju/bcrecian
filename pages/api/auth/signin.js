require("../DB/conn");
const User = require("../models/userSchema.js");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
var Cookies = require("cookies");
const env = require("../config");

export default async function handler(req, res) {
  console.log(req);
  if (req.method === "POST") {
    try {
      let token, userDetails;
      let { email, password } = req.body;

      if (!email || !password) {
        return res.status(422).json({ error: "fill all the fields" });
      }
      email = email.toLowerCase();
      const userLogin = await User.findOne({ email: email });
      if (userLogin) {
        const isMatch = await bcrypt.compare(password, userLogin.password);

        if (isMatch) {
          if (!userLogin.isVerified) {
            return res
              .status(422)
              .json({
                error:
                  "Your account is not verified, please check your mailbox for the verification link.",
              });
          }

          try {
            token = await jwt.sign({ _id: userLogin._id }, env.mpJWTSecretKey);
            userLogin.tokens = userLogin.tokens.concat({ token: token });
            await userLogin.save();

            userDetails = {
              name: userLogin.name,
              roll: userLogin.roll,
              email: userLogin.email,
              isVerified: userLogin.isVerified,
              isAddedDetails: userLogin.isAddedDetails,
              course: userLogin.course,
              semester: userLogin.semester,
              section: userLogin.section,
            };

            // Create a cookies instance
            const cookies = new Cookies(req, res);

            // Set a cookie
            cookies.set("jwt", token, {
              maxAge: 1000000000,
              httpOnly: true, // true by default
            });
          } catch (err) {
            console.log(err);
          }
          res
            .status(200)
            .json({
              message: "user log in successfully",
              userDetails: userDetails,
            });
        } else {
          return res.status(422).json({ error: "Invalid credential" });
        }
      } else {
        return res.status(422).json({ error: "Invalid credential" });
      }
    } catch (err) {
      console.log(err);
      res.status(200).json({ error: "error" }); /// change needed
    }
  } else {
    return res.status(405).json({ error: "Method not allowed" });
  }
}
