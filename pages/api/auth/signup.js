require("../DB/conn");
const User = require("../models/userSchema.js");
const Validation = require("../validation/validator.js");
const sendMail = require("../helpers/sendMail");
const OTP = require("../models/otpSchema");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
var Cookies = require("cookies");
const env = require("../config");

export default async function handler(req, res) {
  // console.log(process.env.mpDB);
  // console.log(process.env.mpEmail);
  // console.log(process.env.mpPassword);
  // console.log(process.env.mpJWTSecretKey);
  if (req.method === "POST") {
    let { name, roll, email, password } = req.body;
    email = email.toLowerCase();

    if (!name || !roll || !email || !password) {
      return res.status(422).json({ error: "fill all the fields" });
    }
    if (!Validation.nameValidation(name)) {
      return res.status(422).json({ error: "Name not valid" });
    }
    if (!Validation.rollValidation(roll)) {
      return res.status(422).json({ error: "Roll not valid" });
    }
    if (!Validation.emailValidation(email)) {
      return res.status(422).json({ error: "Email not valid" });
    }
    if (!Validation.passwordValidation(password)) {
      return res.status(422).json({ error: "Please Set a strong password" });
    }

    try {
      const userExist = await User.findOne({ email: email });
      if (userExist) {
        return res
          .status(422)
          .json({
            error:
              "This email address is already associated with another account.!",
          });
      }
      const rollExist = await User.findOne({ roll: roll });
      if (rollExist) {
        return res.status(422).json({ error: "Roll number already exist!" });
      }

      const user = new User({ name, roll, email, password });
      try {
        const token = jwt.sign({ _id: user._id }, env.mpJWTSecretKey);
        user.tokens = user.tokens.concat({ token: token });

        // Create a cookies instance
        const cookies = new Cookies(req, res);

        // Set a cookie
        cookies.set("jwt", token, {
          maxAge: 1000000000,
          httpOnly: true, // true by default
        });
      } catch (err) {
        console.log(err);
      }

      let otpCode = Math.floor(Math.random() * (9999 - 1000) + 1000);


      let link = await bcrypt.hash(otpCode.toString(), 12);
      link = link.replace(".", "");
      link = link.replace("/", "");
      link = link.replace("$", "");

      const emailLink =
        "http://" + req.headers.host + "/api/auth/confirmemail?link=" + link;
      const emailBody = {
        head: "Your Registration is Completed!",
        body: "Thank you for using College Space . <br> Please, consider this as an confirmation email. Click the below button to verify your account . If you have any questions contact us with this email address ",
      };
      try {
        await sendMail(emailLink, email, name, emailBody);
      } catch (e) {
        console.log(e);
        return res.status(500).json({ error: "user registration failed" });
      }
      let otp = new OTP({ _userId: user._id, otp: link });
      await otp.save();
      const userRegistration = await user.save();

      if (userRegistration) {
        res.status(201).json({ message: "user registered successfully!" });
      } else {
        res.status(500).json({ error: "user registration failed" });
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({ error: "user registration failed" });
    }
  } else {
    return res.status(405).json({ error: "Method not allowed" });
  }
}
