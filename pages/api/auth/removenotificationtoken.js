require("../DB/conn");
const jwt = require("jsonwebtoken");
const Cookies = require("cookies");
const User = require("../models/userSchema");
const env = require("../config");

export default async function handler(req, res) {
    try {
        let notification_token = req.query
        notification_token = notification_token.token
        const cookies = new Cookies(req, res);
        // Get a cookie
        const token = cookies.get("jwt");
        if (token) {
            const verifyUser = jwt.verify(token, env.mpJWTSecretKey);
            console.log(verifyUser);
            const userExist = await User.update({ _id: verifyUser._id }, { $pull: { 'notification_tokens': { 'token': notification_token } } });
            // await userExist.save()
            if (userExist) {
                res.status(200).json({ message: "Token Deleted" });

            }
            else {
                res.status(200).json({ error: "User not exist" });
            }
        } else {
            res.status(200).json({ userDetails: undefined });
        }
    } catch (err) {
        console.log(err);
        res.status(422).json({ error: "some error occurs" });
    }
}
