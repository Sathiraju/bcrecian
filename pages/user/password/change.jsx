import { useRouter } from "next/dist/client/router";
import { useRef, useState } from "react";
import Button from "../../../components/Button/Button";
import Spinner from "../../../components/Spinner/Spinner";
import styles from "./change.module.scss";

const ChangePassword = () => {
  const [isLoading, setLoading] = useState(false);
  const password = useRef();
  const confirmPassword = useRef();
  const router = useRouter();

  const onPasswordChange = async () => {
    setLoading(true);
    const pass = password.current.value;
    const confPass = confirmPassword.current.value;

    if (pass !== confPass) window.alert("Password mismatch !");
    // else if (pass.length < 8) window.alert("Min 8 letter required !");
    else {
      const link = router.query.link;

      try {
        const response = await fetch(
          `http://localhost:3000/api/auth/changepassword?link=${link}&newPassword=${pass}`
        );

        const data = await response.json();
        console.log(data);
      } catch (error) {
        console.log(error.message);
      }
    }
    setLoading(false);
  };

  return (
    <div className={styles.container}>
      <form>
        <fieldset>
          <legend>Change Password</legend>
          <div className={styles.input}>
            <div>New Password</div>
            <input ref={password} type="password" />
          </div>
          <div className={styles.input}>
            <div>Confrim Password</div>
            <input ref={confirmPassword} type="password" />
          </div>
          <Button onClick={onPasswordChange} type="button" disabled={isLoading}>
            {isLoading ? <Spinner /> : "Change"}
          </Button>
        </fieldset>
      </form>
    </div>
  );
};

export default ChangePassword;
