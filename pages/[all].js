/* eslint-disable @next/next/no-img-element */
import { useRouter } from "next/dist/client/router";
const All = () => {
  const router = useRouter();

  return (
    <div style={{ margin: "15rem 6rem", textAlign: "center" }}>
      <p style={{ fontSize: "4rem" }}>
        {router.asPath.slice(1)} page is under construction.
      </p>
      <img
        style={{ width: "45rem", height: "45rem", margin: "2rem 0" }}
        src="https://i.ibb.co/sFKwnt2/OE60-SH0-removebg-preview.png"
        alt="under-working"
      />
      <p>We are working hard, to build it ASAP.</p>
      <p>- Team CollegeSpace</p>
    </div>
  );
};

export default All;
