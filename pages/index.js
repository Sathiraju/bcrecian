import { Fragment } from "react";
import Grid from "../components/Grid/Grid";
import { EndPoint } from "../helpers/api/ENDPOINT";
import Meta from "../meta/Meta";

export default function Home() {
  return (
    <Fragment>
      <Meta />
      <Grid />
    </Fragment>
  );
}
