import Head from "next/head";
import { useRouter } from "next/router";

const Meta = ({
  title = "College Space",
  description = "Home Page of College Space. You can download notes, look through noties, get class updated here",
}) => {
  const route = useRouter();

  return (
    <Head>
      <title>{title}</title>
      <meta
        name="google-site-verification"
        content="RLnyRIyVGk-qBnUZvhzHrnCXICEbP1k8XsEWJZGi0jw"
      />
      <meta name="description" content={description} />
      <meta charSet="UTF-8" />
      <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <meta property="og:site_name" content="College Space" />
      <meta property="og:url" content={route.pathname} />
      <meta property="og:title" content={title} />
      <meta
        property="og:image"
        content="https://i.ibb.co/JvTkbCL/3700-10-02-ai.jpg"
      />
      <meta property="og:description" content={description} />
      <link rel="shortcut icon" href="/icon-512x512.png" />
    </Head>
  );
};

export default Meta;
