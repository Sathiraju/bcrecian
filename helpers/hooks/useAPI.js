import { useCallback, useState } from "react";
import { useIsMounted } from "./useIsMounted";

const useAPI = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const isMounted = useIsMounted();

  const sendRequest = useCallback(
    async (requestConfig) => {
      // console.log(requestConfig);
      setIsLoading(true);
      setError(null);

      try {
        const response = await fetch(requestConfig.url, {
          method: requestConfig.method ? requestConfig.method : "GET",
          headers: requestConfig.headers ? requestConfig.headers : {},
          body: requestConfig.body ? JSON.stringify(requestConfig.body) : null,
        });

        const data = await response.json();

        if (!response.ok) {
          throw new Error(data.error || "Request failed!");
        }
        return data;
      } catch (err) {
        if (isMounted.current) {
          setError(err.message);
          setIsLoading(false);
        }
      }
    },
    [isMounted]
  );

  return { isLoading, error, sendRequest };
};

export default useAPI;
