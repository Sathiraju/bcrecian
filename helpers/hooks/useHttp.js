import { useCallback, useState } from "react";
import { useIsMounted } from "./useIsMounted";

const useHttp = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const isMounted = useIsMounted();

  const sendRequest = useCallback(
    async (requestConfig, applyData) => {
      setIsLoading(true);
      setError(null);

      try {
        const response = await fetch(requestConfig.url, {
          method: requestConfig.method ? requestConfig.method : "GET",
          headers: requestConfig.headers ? requestConfig.headers : {},
          body: requestConfig.body ? JSON.stringify(requestConfig.body) : null,
        });

        if (!response.ok) {
          throw new Error("Request failed!");
        }

        const data = await response.json();
        // if component unmounted, setstate will not perfrom.
        if (isMounted.current) {
          setIsLoading(false);
          applyData(data);
        }
      } catch (err) {
        console.log(err.message || "Something went wrong !");
        if (isMounted.current) {
          setError(err);
          setIsLoading(false);
        }
      }
    },
    [isMounted]
  );

  return { isLoading, error, sendRequest };
};

export default useHttp;
