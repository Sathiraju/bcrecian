const isOnGoing = (start, end) => {
  const [start_hour, start_min] = start.split(":").map(Number);
  const [end_hour, end_min] = end.split(":").map(Number);
  const current_min = new Date().getHours() * 60 + new Date().getMinutes();

  if (
    current_min >= start_hour * 60 + start_min &&
    current_min <= end_hour * 60 + end_min
  ) {
    return true;
  }
};

export default isOnGoing;
