import { useState, useCallback } from "react";
import { useSelector } from "react-redux";
import { EndPoint } from "../../helpers/api/ENDPOINT";
import { IoMdNotificationsOutline, IoMdNotifications } from "react-icons/io";
import useHttp from "../../helpers/hooks/useHttp";
import NotifyBar from "./NotifyBar";

import styles from "./NotifyBar.module.scss";
import { useEffect } from "react";

import { initializeApp } from "firebase/app";
import { getMessaging, onMessage } from "firebase/messaging";
import useFocus from "../../helpers/hooks/useFocus";

const firebaseConfig = {
  apiKey: "AIzaSyDC94VsmZh0YWkcrdeR_s8WZtg712DP5Bw",
  authDomain: "django-notification-b0084.firebaseapp.com",
  projectId: "django-notification-b0084",
  storageBucket: "django-notification-b0084.appspot.com",
  messagingSenderId: "608576668692",
  appId: "1:608576668692:web:f6a710d7d3346ec8e1d301",
  measurementId: "G-Y98CRFQ6Z6",
};

const NotifyIcon = () => {
  const isFocus = useFocus();
  const [notiCount, setNotiCount] = useState(0);
  const [isOpen, setIsOpen] = useState(false);
  const { email } = useSelector((state) => state.user);
  const { isLoading, error, sendRequest } = useHttp();
  const app = initializeApp(firebaseConfig);
  const messaging = getMessaging(app);

  const getNotiCount = useCallback(() => {
    const requestConfig = {
      url: `${EndPoint}/get-new-notification-count/`,
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      body: { email: email },
    };
    sendRequest(requestConfig, setNotiCount);
  }, [email, sendRequest]);

  // Recall notication count at start, and also if new notification arries.
  useEffect(() => {
    getNotiCount();
    onMessage(messaging, (payload) => {
      getNotiCount();
    });
  }, [messaging, getNotiCount]);

  // Recall notification count, if user back into the TAB
  useEffect(() => {
    if (isFocus) getNotiCount();
  }, [isFocus, getNotiCount]);

  const handleModal = () => {
    setIsOpen((prev) => !prev);
  };

  return (
    <>
      <span onClick={handleModal} className={styles.iconBox}>
        {!isOpen && !isLoading && notiCount !== 0 && (
          <div className={styles.count}>{notiCount}</div>
        )}
        {isOpen ? <IoMdNotifications /> : <IoMdNotificationsOutline />}
      </span>
      {isOpen && (
        <NotifyBar
          closeModal={handleModal}
          getNotiCount={getNotiCount}
          isOpen={isOpen}
          count={notiCount}
        />
      )}
    </>
  );
};

export default NotifyIcon;
