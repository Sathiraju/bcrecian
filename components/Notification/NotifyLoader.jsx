/* eslint-disable @next/next/no-img-element */
import styles from "./NotifyBar.module.scss";

const NotifyLoader = () => {
  return (
    <ul>
      {new Array(7).fill(0).map((each, index) => (
        <li className={styles.dummyContainer} key={index}>
          <div className={styles.dummyImg} />
          <aside>
            <div className={styles.dummyHeading}> </div>
            <p className={styles.dummyText} />
            <p className={styles.dummyText} />
            <p className={styles.dummyText} />
          </aside>
        </li>
      ))}
    </ul>
  );
};

export default NotifyLoader;
