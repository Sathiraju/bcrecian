import { useState, useEffect, useRef, useCallback } from "react";
import styles from "./Modal.module.scss";
import ReactDOM from "react-dom";
import { IoCloseSharp } from "react-icons/io5";
import CancelBtn from "../CancelButton/CancelBtn";

export default function Modal({ closeModal, isOpen, children }) {
  const overlayRef = useRef();
  const [isBrowser, setIsBrowser] = useState(false);

  useEffect(() => {
    setIsBrowser(true);
  }, []);

  const handleClick = (event) => {
    if (overlayRef.current === event.target) closeModal();
  };

  // Close the modal pressing Escape key
  const keyDownCheck = useCallback(
    (e) => {
      if (e.key === "Escape" && isOpen) closeModal();
    },
    [closeModal, isOpen]
  );

  useEffect(() => {
    document.addEventListener("keydown", keyDownCheck);

    return () => {
      document.removeEventListener("keydown", keyDownCheck);
    };
  }, [keyDownCheck]);

  const modalContent = (
    <div className={styles.overlay} ref={overlayRef} onClick={handleClick}>
      <div className={styles.modal}>
        <div className={styles.header}>
          <CancelBtn closeModal={closeModal} />
        </div>
        <div className={styles.body}>{children}</div>
      </div>
    </div>
  );

  if (isBrowser) {
    return ReactDOM.createPortal(
      modalContent,
      document.getElementById("modal-root")
    );
  } else {
    return null;
  }
}
