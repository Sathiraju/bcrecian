import { useRef } from "react";
import { useCallback, useEffect, useState } from "react";
import { ImSearch } from "react-icons/im";
import { IoMdCloseCircle } from "react-icons/io";
import Results from "./Results";

import styles from "./Searchbar.module.scss";

const Searchbar = () => {
  const [isFocus, setFocus] = useState(false);
  const [searchText, setSearchText] = useState("");
  const searchRef = useRef();

  const handleFocus = () => {
    setFocus(true);
    searchRef.current.focus();
  };

  const onCancel = () => {
    setFocus(false);
    setSearchText("");
    searchRef.current.blur(); // remove Focus
  };

  const handleSearch = (e) => {
    setSearchText(e.target.value);
  };

  // Close the modal pressing Escape key
  const keyDownCheck = useCallback(
    (e) => {
      if (e.key === "Escape" && isFocus) onCancel();
      if (e.altKey && e.key.toLowerCase() === "k" && !isFocus) handleFocus();
    },
    [isFocus]
  );

  useEffect(() => {
    document.addEventListener("keydown", keyDownCheck);

    return () => {
      document.removeEventListener("keydown", keyDownCheck);
    };
  }, [keyDownCheck]);

  return (
    <>
      <div className={styles.searchBar}>
        <input
          type="text"
          placeholder="Search Notes"
          value={searchText}
          onFocus={handleFocus}
          onChange={handleSearch}
          ref={searchRef}
        />
        <div className={styles.searchIcon}>
          {isFocus ? (
            <IoMdCloseCircle onClick={onCancel} className={styles.largeIcon} />
          ) : (
            <ImSearch />
          )}
        </div>
      </div>
      {isFocus && <Results searchText={searchText} />}
    </>
  );
};

export default Searchbar;
