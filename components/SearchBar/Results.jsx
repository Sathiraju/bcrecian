import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { EndPoint } from "../../helpers/api/ENDPOINT";
import useHttp from "../../helpers/hooks/useHttp";
import Container from "../Container/Container";
import Note from "../Grid/Notes/Note";
import Spinner from "../Spinner/Spinner";

import styles from "./Searchbar.module.scss";
import classes from "../Grid/Grid.module.scss";
import noteStyles from "../Grid/Notes/Index.module.scss";

const Hints = (
  <div className={styles.hints}>
    <p>You can search notes, using Paper Name, Paper Code and Dates. </p>
    <p>
      Date should be in, <span>YYYY-MM-DD</span> Format
    </p>
    <div className={styles.smallText}>
      <p>
        Quick Tip : You can use{" "}
        <span className={styles.highlight}>Alt + k </span> for quick search.
      </p>
      <p>
        Pro Tip : Use <span className={styles.highlight}>Esc</span> for exit
        from search window. from the search.
      </p>
    </div>
  </div>
);

const Results = ({ searchText }) => {
  const [notes, setNotes] = useState(null);
  const { isLoading, error, sendRequest: searchNotes } = useHttp();
  const { course, semester, section } = useSelector((state) => state.user);

  useEffect(() => {
    // The api will call after 1 sec, when user stops typing.
    const calledAfter = setTimeout(() => {
      const requestConfig = {
        url: `${EndPoint}/allnotes/`,
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: {
          course,
          semester,
          section,
          keyword: searchText,
        },
      };

      if (searchText !== "") {
        searchNotes(requestConfig, setNotes);
      }
    }, 1000);

    return () => {
      clearTimeout(calledAfter);
    };
  }, [searchText, course, semester, section, searchNotes]);

  return (
    <div className={styles.search_overlay}>
      <div className={styles.container}>
        {isLoading && <Spinner />}
        {!notes && !isLoading && Hints}
        {!isLoading && !error && notes?.message && (
          <div className={styles.hints}>
            <p>{notes.message}</p>
          </div>
        )}
        {!isLoading &&
          notes?.payload &&
          Object.keys(notes.payload).map((note) => (
            <Container key={note}>
              <h3 className={classes.date}>{note}</h3>
              <div className={noteStyles.notesContainer}>
                {notes.payload[note].map((paper) => (
                  <Note key={paper.paper_code.code} paper={paper} />
                ))}
              </div>
            </Container>
          ))}
      </div>
    </div>
  );
};

export default Results;
