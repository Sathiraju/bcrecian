import Link from "next/link";

const Container = ({ title, items }) => {
  return (
    <div>
      <p>{title}</p>
      <ul>
        {items.map((each, index) => (
          <Link key={index} href="/more/logbook" passHref>
            <li>{each}</li>
          </Link>
        ))}
      </ul>
    </div>
  );
};

export default Container;
