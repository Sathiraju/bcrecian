import Container from "./Container";
import styles from "./MoreContainer.module.scss";

const profile = [
  "Update Profile",
  "Update Details",
  "Reset Password",
  "Two-step Verification",
  "Change Email address",
  "Apply for CR",
];

const features = [
  "Class LogBook",
  "Pevious Year Questions",
  "Timetable",
  "Assignments",
  "CA & Practical Marks",
  "Professors Details",
];

const extras = [
  "About Us",
  "Sitemap",
  "Contribute to this Project",
  "Project Synopsis",
  "Report a bug",
  "Contact Us",
];

const MoreContainer = () => {
  return (
    <>
      <div className={styles.container}>
        <Container title="Profile Section" items={profile} />
        <Container title="Features" items={features} />
        <Container title="Extras" items={extras} />
      </div>
      <p className={styles.copyright}>
        Ⓒ ClgSpace 2021. All rights are reserved.
      </p>
    </>
  );
};

export default MoreContainer;
