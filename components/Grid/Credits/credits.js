const CREDITS = [
  {
    name: "Raj Kar",
    link: "https://github.com/Raj-kar",
    work: "Full Stack Dev",
  },
  {
    name: "Ananda Gopal Dutta",
    link: "https://www.facebook.com/ananda.g.dutta",
    work: "Backend Dev + Database Admin",
  },
  {
    name: "Shreya Das",
    link: "https://github.com/Shreya-Das74",
    work: "Database Admin",
  },
  {
    name: "Samwit Adhikary",
    link: "https://github.com/SamwitAdhikary",
    work: "Tester + Analytics",
  },
  {
    name: "Sneha Majumdar",
    link: "https://www.facebook.com/sneha.majumder.549668",
    work: "Tester + Analytics",
  },
  {
    name: "Bishaka Rao",
    link: "https://www.facebook.com/bisakha.rao.24",
    work: "Tester + Analytics",
  },
  {
    name: "Souvik Sen",
    link: "https://www.facebook.com/souvik.sen.5245961",
    work: "Tester + Analytics",
  },
  {
    name: "Subhajit Choudhary",
    link: "https://www.facebook.com/subhajit.chowdhury.182940",
    work: "Tester + Analytics",
  },
  {
    name: "catalyststuff",
    link: "https://www.freepik.com/free-vector/astronaut-reading-book-beanbag-cartoon-vector-icon-illustration_16551062.htm#page=1&query=college%20icon&position=41&from_view=search",
    work: "App Icon",
  },
];

export default CREDITS;
