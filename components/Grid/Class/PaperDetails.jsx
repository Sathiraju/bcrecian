import { trimTime } from "./ClassList";
import styles from "./PaperDetails.module.scss";

const PaperDetails = ({ data }) => {
  return (
    <div>
      <p className={styles.heading}>Paper Details</p>
      <div className={styles.container}>
        {Object.keys(data.paper).map((key) => {
          return (
            <div key={key} className={styles.details}>
              <p>{key}</p>
              <p>{data.paper[key]}</p>
            </div>
          );
        })}
      </div>
      <div className={styles.container}>
        <div className={styles.details}>
          <p>Taken By</p>
          <p>
            {data.teacher.name} ({data.teacher.slug})
          </p>
        </div>
        <div className={styles.details}>
          <p>Link</p>
          <p>
            {data.teacher.has_permanent_link ? (
              <a
                target="_blank"
                rel="noreferrer"
                href={data.teacher.class_link}
              >
                Join Class
              </a>
            ) : (
              "no permanent link."
            )}
          </p>
        </div>
      </div>
      <div className={styles.container}>
        <div className={styles.details}>
          <p>Start at</p>
          <p>{trimTime(data.start_time)}</p>
        </div>
        <div className={styles.details}>
          <p>End at</p>
          <p>{trimTime(data.end_time)}</p>
        </div>
      </div>
      {data.is_cancel && (
        <div className={styles.postponed}>
          <p>Class is Postponed For Today.</p>
        </div>
      )}
    </div>
  );
};

export default PaperDetails;
