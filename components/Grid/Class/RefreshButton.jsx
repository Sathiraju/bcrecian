import { forwardRef } from "react";
import { MdRefresh } from "react-icons/md";
import classes from "./Class.module.scss";

// eslint-disable-next-line react/display-name
const RefreshButton = forwardRef(({ isLoaing, handleFetch }, ref) => {
  return (
    <div ref={ref}>
      <MdRefresh
        className={isLoaing ? classes.loading : ""}
        onClick={handleFetch}
      />
    </div>
  );
});

export default RefreshButton;
