import { IoIosArrowBack, IoIosArrowForward } from "react-icons/io";
import Spinner from "../../Spinner/Spinner";
import ClassList from "./ClassList";
import styles from "./Class.module.scss";

const Routine = ({ isLoading, routine, setRoutine, setCurTime }) => {
  return (
    <div className={styles.routine}>
      <IoIosArrowBack className="icon" />
      {isLoading ? <Spinner /> : <ClassList timetable={routine?.payload} />}
      <IoIosArrowForward className="icon" />
    </div>
  );
};

export default Routine;
