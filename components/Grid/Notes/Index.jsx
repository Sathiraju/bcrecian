import { Fragment, useEffect, useState } from "react";
import useHttp from "../../../helpers/hooks/useHttp";
import Note from "./Note";
import Container from "../../Container/Container";
import { useSelector } from "react-redux";
import Spinner from "../../Spinner/Spinner";
import classes from "./Index.module.scss";
import { EndPoint } from "../../../helpers/api/ENDPOINT";

import styles from "../Grid.module.scss";

const Notes = () => {
  const { isLoading, error, sendRequest } = useHttp();
  const [notes, setNotes] = useState(null);
  const { course, semester, section } = useSelector((state) => state.user);

  useEffect(() => {
    const requestConfig = {
      url: `${EndPoint}/allnotes/?course=${course}&semester=${semester}&section=${section}`,
    };
    sendRequest(requestConfig, setNotes);
  }, [sendRequest, course, semester, section]);

  return (
    <Fragment>
      <div className={classes.center}>
        {!isLoading && error && <p>{error.message}</p>}
        {isLoading && (
          <Fragment>
            <Spinner />
            <p>Fetching Notes ....</p>
          </Fragment>
        )}
        {!isLoading && notes?.message && <p>{notes.message}</p>}
      </div>
      {!isLoading &&
        notes?.payload &&
        Object.keys(notes.payload).map((note) => (
          <Container key={note}>
            <h3 className={styles.date}>{note}</h3>
            <div className={classes.notesContainer}>
              {notes.payload[note].map((paper) => (
                <Note key={paper.paper_code.code} paper={paper} />
              ))}
            </div>
          </Container>
        ))}
    </Fragment>
  );
};

export default Notes;
