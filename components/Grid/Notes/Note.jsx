import Button from "../../Button/Button";
import styles from "./Index.module.scss";

const Note = ({ paper }) => {
  const { code, name } = paper.paper_code;

  return (
    <div className={styles.note}>
      <p> {code} </p>
      <p> {name} </p>
      <a target="_blank" rel="noreferrer" href={paper.file_link}>
        <Button> Download </Button>
      </a>
    </div>
  );
};

export default Note;
