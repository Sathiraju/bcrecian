import styles from "./Grid.module.scss";
import Notes from "./Notes/Index";
import Class from "./Class/Class";
import Announcements from "./Announcements";
import Section from "../Section/Section";

import { MdAnnouncement, MdClass } from "react-icons/md";
import { CgNotes } from "react-icons/cg";
// =======================================
import { initializeApp } from "firebase/app";
import { getMessaging, getToken } from "firebase/messaging";

const firebaseConfig = {
  apiKey: "AIzaSyDC94VsmZh0YWkcrdeR_s8WZtg712DP5Bw",
  authDomain: "django-notification-b0084.firebaseapp.com",
  projectId: "django-notification-b0084",
  storageBucket: "django-notification-b0084.appspot.com",
  messagingSenderId: "608576668692",
  appId: "1:608576668692:web:f6a710d7d3346ec8e1d301",
  measurementId: "G-Y98CRFQ6Z6"
};
let count = 0;
const Grid = () => {
  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const messaging = getMessaging(app)
  if (count <= 0) {
    getToken(messaging, { vapidKey: 'BFGvJo4hkN1AQu-AilOOvQLqqfgAn0-GO0ETs8fo9ZQ2I3j0CL1ucDzEJw86bYx6LDsx6YjyNQryXoMOS4StKas' }).then((currentToken) => {
      if (currentToken) {
        console.log(currentToken)
        fetch('/api/auth/addnotificationtoken?token=' + currentToken).then((res => {
          console.log(res.json())
        })).catch((err) => {
          console.log(err)
        })
      } else {
        console.log('No registration token available. Request permission to generate one.');

      }
    }).catch((err) => {
      console.log('An error occurred while retrieving token. ', err);
    });
  }
  count++;



  return (
    <main>
      <section className={styles.home}>
        <div className={styles.left}>
          <Section title="Notes" Icon={CgNotes} Component={Notes} />
        </div>
        <div className={styles.right}>
          <div>
            <Section title="Classes" Icon={MdClass} Component={Class} />
          </div>
          <div>
            <Section
              title="Announcements"
              Icon={MdAnnouncement}
              Component={Announcements}
            />
          </div>
        </div>
      </section>
    </main>
  );
};

export default Grid;
