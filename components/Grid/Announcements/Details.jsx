import { Fragment } from "react";
import styles from "./Details.module.scss";

const Details = ({ data }) => {
  return (
    <Fragment>
      <p className={styles.heading}>Details</p>
      <div className={styles.container}>
        <p className={styles.title}>{data.title}</p>
        <div className={styles.body}>
          <p dangerouslySetInnerHTML={{ __html: data.body }}></p>
          <div className={styles.extras}>
            <p>{data.upload_date_time}</p>
            <p>- By {data.upload_by}</p>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default Details;
