import styles from "./UserProfile.module.scss";

const Component = ({ msg, data }) => {
  return (
    <div className={styles.container}>
      <p>{msg}</p>
      <p>{data}</p>
    </div>
  );
};

export default Component;
