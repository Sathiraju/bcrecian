import { Fragment } from "react";
import { useSelector } from "react-redux";
import LogoutButton from "./LogoutButton";
import Component from "./Component";

import styles from "./UserProfile.module.scss";

const stripData = (data) => {
  const len = data.length;
  return len > 23 ? `${data.slice(0, 15)}...${data.slice(len - 3, len)}` : data;
};

const UserProfile = () => {
  const { course, email, name, roll, section, semester } = useSelector(
    (state) => state.user
  );

  return (
    <Fragment>
      <p className={styles.heading}>Profile</p>
      <div className={styles.profile}>
        <Component msg="Name" data={stripData(name)} />
        <Component msg="Email" data={stripData(email)} />
        <Component msg="Roll" data={roll} />
        <Component msg="Course" data={course} />
        <Component msg="Semester" data={semester} />
        <Component msg="Section" data={section} />
        <LogoutButton />
      </div>
    </Fragment>
  );
};

export default UserProfile;
