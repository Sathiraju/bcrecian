import { AiFillSetting } from "react-icons/ai";
import { FaBook } from "react-icons/fa";
import { HiHome } from "react-icons/hi";
import { ImBullhorn } from "react-icons/im";

export const MENU_DATA = [
  {
    title: "Home",
    link: "/",
    icon: <HiHome />,
  },
  {
    title: "Notes",
    link: "/notes",
    icon: <FaBook />,
  },
  {
    title: "Jobs",
    link: "/jobs",
    icon: <ImBullhorn />,
  },
  {
    title: "More",
    link: "/more",
    icon: <AiFillSetting />,
  },
];

export default MENU_DATA;
