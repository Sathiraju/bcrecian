import Link from "next/link";
import styles from "./Navigation.module.scss";
import MENU_DATA from "./NavData";
import { useRouter } from "next/dist/client/router";

const MobileNavigation = () => {
  const router = useRouter(); 

  return (
    <div className={styles.container}>
      <ul>
        {MENU_DATA.map((menu) => (
          <Link key={menu.title} href={menu.link} passHref>
            <li className={menu.link === router.asPath ? styles.active : ""}>
              {menu.icon}
              {menu.title}
            </li>
          </Link>
        ))}
      </ul>
    </div>
  );
};

export default MobileNavigation;
