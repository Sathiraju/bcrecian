import { Fragment, useRef } from "react";
import { Si1Password } from "react-icons/si";
import useAPI from "../../helpers/hooks/useAPI";
import Spinner from "../Spinner/Spinner";
import InputBox from "./InputBox";

import styles from "./Form.module.scss";
import { ACTIONS } from "./formReducer";
import ResendOtp from "./ResendOtp";

const EnterOtp = ({ closeModal, formDispatch, state }) => {
  const { isLoading, error, sendRequest } = useAPI();
  const otpRef = useRef(null);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const otp = parseInt(otpRef.current.value);

    const requestConfig = {
      url: "/api/auth/checkotp",
      method: "POST",
      headers: { "content-type": "application/json" },
      body: {
        email: state.email,
        otp,
      },
    };

    const data = await sendRequest(requestConfig);
    if (data) {
      formDispatch({ type: ACTIONS.SetPassword, payload: { otp } });
    }
  };

  return (
    <Fragment>
      <p className="modalHeading">VERIFY OTP</p>
      <form className={styles.form} onSubmit={handleSubmit}>
        <InputBox
          id="user_otp"
          Icon={Si1Password}
          title="Enter the OTP"
          type="text"
          inputRef={otpRef}
        />
        <p>
          A four digit OTP is send to your email <b>{state.email}</b>.
        </p>
        {error && <p className={styles.errText}>{error}</p>}
        {isLoading ? (
          <Spinner />
        ) : (
          <>
            <button>Submit</button>
            <ResendOtp formDispatch={formDispatch} state={state} />{" "}
          </>
        )}
      </form>
      {!isLoading && (
        <div className="extras">
          <p onClick={() => formDispatch({ type: ACTIONS.Login })}>Login</p>
          <p>I also forgot my EMAIL</p>
        </div>
      )}
    </Fragment>
  );
};

export default EnterOtp;
