import { Fragment, useRef, useState } from "react";
import { RiLockPasswordFill } from "react-icons/ri";
import useAPI from "../../helpers/hooks/useAPI";
import Spinner from "../Spinner/Spinner";
import InputBox from "./InputBox";
import { ACTIONS } from "./formReducer";
import { validatePassword } from "../../helpers/register/validate";

import styles from "./Form.module.scss";

const NewPassword = ({ closeModal, formDispatch, state }) => {
  const { isLoading, error, sendRequest } = useAPI();
  const passwordRef = useRef(null);
  const [success, setSuccess] = useState(false);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const password = passwordRef.current.value;
    const hasError = validatePassword(password);

    if (hasError) {
      formDispatch({ type: ACTIONS.SetError, payload: { error: hasError } });
    } else {
      const requestConfig = {
        url: "/api/auth/changepassword",
        method: "POST",
        headers: { "content-type": "application/json" },
        body: {
          email: state.email,
          otp: state.otp,
          newPassword: password,
        },
      };
      const data = await sendRequest(requestConfig);
      if (data) setSuccess(true);
    }
  };

  return (
    <Fragment>
      <p className="modalHeading">New Password</p>
      {success ? (
        <div className={styles.success}>
          <p>Password Changed Successfully.</p>
          <p>Now you can Login with your new password.</p>
          <p>A confirmation mail has send to your email {state.email}.</p>
        </div>
      ) : (
        <form className={styles.form} onSubmit={handleSubmit}>
          <InputBox
            id="user_password"
            Icon={RiLockPasswordFill}
            title="Enter your new password"
            type="password"
            inputRef={passwordRef}
          />
          {error && <p className={styles.errText}>{error}</p>}
          {state.error && <p className={styles.errText}>{state.error}</p>}
          {isLoading ? <Spinner /> : <button>Change Password</button>}
        </form>
      )}
      {!isLoading && (
        <div className="extras">
          <p onClick={() => formDispatch({ type: ACTIONS.Login })}>Login</p>
        </div>
      )}
    </Fragment>
  );
};

export default NewPassword;
