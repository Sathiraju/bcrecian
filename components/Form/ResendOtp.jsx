import { Fragment, useEffect, useState } from "react";
import useAPI from "../../helpers/hooks/useAPI";
import Spinner from "../Spinner/Spinner";

import styles from "./Form.module.scss";

const ResendOtp = ({ formDispatch, state }) => {
  const { isLoading, error, sendRequest } = useAPI();
  const [time, setTime] = useState(60);
  const [success, setSuccess] = useState(false);

  useEffect(() => {
    const timeOut = setTimeout(() => {
      setTime((time) => time - 1);
    }, 1000);
    if (time === 0) clearTimeout(timeOut);

    return () => {
      clearTimeout(timeOut);
    };
  });

  const resendOtp = async () => {
    const requestConfig = {
      url: "/api/auth/resendotp",
      method: "POST",
      headers: { "content-type": "application/json" },
      body: {
        email: state.email,
      },
    };

    const data = await sendRequest(requestConfig);
    console.log(data);
    if (data) setSuccess(true);
  };

  return (
    <Fragment>
      {error && <p className={styles.errText}>{error}</p>}
      {success ? (
        <div style={{ textAlign: "center", marginTop: "1rem" }}>
          <p>A new OTP send Successfully.</p>
        </div>
      ) : time <= 0 ? (
        isLoading ? (
          <Spinner />
        ) : (
          <button onClick={resendOtp}>Resend OTP</button>
        )
      ) : (
        <div style={{ textAlign: "center", marginTop: "1rem" }}>
          <p>You can resend otp after {time} sec.</p>
        </div>
      )}
    </Fragment>
  );
};

export default ResendOtp;
