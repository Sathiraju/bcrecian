import { useRouter } from "next/router";
import { Fragment, useRef, useState } from "react";
import { AiFillSafetyCertificate } from "react-icons/ai";
import { BiUserCircle } from "react-icons/bi";
import { MdEmail } from "react-icons/md";
import { RiLockPasswordFill } from "react-icons/ri";
import { useDispatch } from "react-redux";
import { validateData } from "../../helpers/register/validate";
import { userActions } from "../../store/user";
import Spinner from "../Spinner/Spinner";
import styles from "./Form.module.scss";
import InputBox from "./InputBox";

const registerUser = async (name, roll, email, password) => {
  try {
    const response = await fetch("/api/auth/signup", {
      method: "POST",
      body: JSON.stringify({
        name,
        roll,
        email,
        password,
      }),
      headers: { "content-type": "application/json" },
    });
    return await response.json();
  } catch (error) {
    console.log(error);
  }
};

const RegisterForm = () => {
  const nameRef = useRef(null);
  const rollRef = useRef(null);
  const emailRef = useRef(null);
  const passwordRef = useRef(null);
  const [isLoading, setLoading] = useState(false);
  const [hasError, setError] = useState(null);
  const router = useRouter();
  const dispatch = useDispatch();

  const handleSubmit = async (event) => {
    event.preventDefault();
    setLoading(true);
    const name = nameRef.current.value;
    const roll = rollRef.current.value;
    const email = emailRef.current.value;
    const password = passwordRef.current.value;

    const err = validateData(name, roll, email, password);

    if (!err) {
      const data = await registerUser(name, roll, email, password);
      setLoading(false);
      if (!data.error) {
        dispatch(userActions.setUserData({ name, roll, email, password }));
        router.replace("/");
      } else {
        setError(data.error);
      }
    } else {
      setError(err);
      setLoading(false);
    }
  };

  return (
    <Fragment>
      <p className="modalHeading">Register</p>
      <form className={styles.form} onSubmit={handleSubmit}>
        <InputBox
          id="user_name"
          Icon={BiUserCircle}
          title="Enter your name"
          type="text"
          inputRef={nameRef}
        />
        <InputBox
          id="user_roll"
          Icon={AiFillSafetyCertificate}
          title="Enter your university roll no"
          type="text"
          inputRef={rollRef}
        />
        <InputBox
          id="user_email"
          Icon={MdEmail}
          title="Enter your email"
          type="email"
          inputRef={emailRef}
        />
        <InputBox
          id="user_password"
          Icon={RiLockPasswordFill}
          title="Enter your password"
          type="password"
          inputRef={passwordRef}
        />
        {!isLoading && hasError && <p className={styles.errText}>{hasError}</p>}
        {isLoading ? <Spinner /> : <button>Register</button>}
      </form>
    </Fragment>
  );
};

export default RegisterForm;
