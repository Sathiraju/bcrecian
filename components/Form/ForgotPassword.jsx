import { Fragment, useRef } from "react";
import { MdEmail } from "react-icons/md";
import useAPI from "../../helpers/hooks/useAPI";
import Spinner from "../Spinner/Spinner";
import InputBox from "./InputBox";

import styles from "./Form.module.scss";
import { ACTIONS } from "./formReducer";

const ForgotPassword = ({ closeModal, formDispatch }) => {
  const { isLoading, error, sendRequest } = useAPI();
  const emailRef = useRef(null);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const email = emailRef.current.value.toLowerCase();

    const requestConfig = {
      url: "/api/auth/forgetpassword",
      method: "POST",
      headers: { "content-type": "application/json" },
      body: {
        email,
      },
    };

    const data = await sendRequest(requestConfig);

    if (data) {
      formDispatch({ type: ACTIONS.SendOtp, payload: { email } });
    }
  };

  return (
    <Fragment>
      <p className="modalHeading">Forgot Password</p>
      <form className={styles.form} onSubmit={handleSubmit}>
        <InputBox
          id="user_email"
          Icon={MdEmail}
          title="Enter your registred email"
          type="email"
          inputRef={emailRef}
        />
        {error && <p className={styles.errText}>{error}</p>}
        {isLoading ? <Spinner /> : <button>Send Otp</button>}
      </form>
      {!isLoading && (
        <div className="extras">
          <p onClick={() => formDispatch({ type: ACTIONS.Login })}>Login</p>
          <p onClick={() => formDispatch({ type: ACTIONS.ForgotEmail })}>
            I also forgot my Email
          </p>
        </div>
      )}
    </Fragment>
  );
};

export default ForgotPassword;
