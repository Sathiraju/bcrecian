import router, { useRouter } from "next/router";
import { Fragment, useEffect, useRef, useState } from "react";
import { useDispatch } from "react-redux";
import { userActions } from "../../store/user";
import Spinner from "../Spinner/Spinner";
import styles from "./Form.module.scss";

const getCourseDetails = async () => {
  const response = await fetch("/api/course/getcourse");
  const data = await response.json();
  return data.courses;
};

const addUserData = async (course, semester, section) => {
  const response = await fetch("/api/auth/adduserdetails", {
    method: "POST",
    body: JSON.stringify({
      course,
      semester,
      section,
    }),
    headers: { "content-type": "application/json" },
  });

  const data = await response.json();
  return data;
};

const UserDetails = () => {
  const [courseData, setCourseData] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useDispatch();

  //   refs
  const courseRef = useRef();
  const semRef = useRef();
  const secRef = useRef();

  useEffect(() => {
    let isMount = true;

    const fetchCourses = async () => {
      const data = await getCourseDetails();
      if (isMount) setCourseData(data);
    };

    fetchCourses();

    return () => {
      isMount = false;
    };
  }, []);

  const onSubmitHandler = async (event) => {
    setIsLoading(true);
    event.preventDefault();
    const course = courseRef.current.value;
    const semester = semRef.current.value;
    const sec = secRef.current.value;

    const data = await addUserData(course, semester, sec);

    if (data.message === "Information saved") {
      setIsLoading(false);
      dispatch(userActions.setUserData({ ...data.userDetails }));
      router.replace("/");
    } else {
      console.log("something went wrong !");
    }
  };

  if (!courseData) return <Spinner />;

  return (
    <Fragment>
      <p className="modalHeading">ADD DETAILS</p>
      <form className={styles.form} onSubmit={onSubmitHandler}>
        <div className={styles.selectBox}>
          <label>Choose your course</label>
          <select ref={courseRef} required>
            {courseData.map((course) => {
              return (
                <option key={course._id} value={course.courseName}>
                  {course.courseName}
                </option>
              );
            })}
          </select>
        </div>
        <div className={styles.selectBox}>
          <label>Choose your semester</label>
          <select ref={semRef} required>
            {Array(6)
              .fill(0)
              .map((_, index) => {
                return (
                  <option key={index} value={index + 1}>
                    {index + 1}
                  </option>
                );
              })}
          </select>
        </div>
        <div className={styles.selectBox}>
          <label>Choose your section</label>
          <select ref={secRef} required>
            <option value="A">A</option>
            <option value="B">B</option>
          </select>
        </div>
        {isLoading ? <Spinner /> : <button>Add Details</button>}
      </form>
    </Fragment>
  );
};

export default UserDetails;
