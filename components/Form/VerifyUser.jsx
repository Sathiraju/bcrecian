import { useRouter } from "next/router";
import { Fragment, useState } from "react";
import { HiOutlineMail } from "react-icons/hi";
import { MdNavigateNext } from "react-icons/md";
import { useDispatch, useSelector } from "react-redux";
import { userActions } from "../../store/user";
import Spinner from "../Spinner/Spinner";
import styles from "./Form.module.scss";
import LogoutButton from "../UserProfile/LogoutButton";

const verifyUserStatus = async () => {
  const response = await fetch("/api/auth/isauthorized/");
  const data = await response.json();
  return data.userDetails;
};

const VerifyUser = () => {
  const { name } = useSelector((state) => state.user);
  const { email } = useSelector((state) => state.user);
  const [isNotVerified, setIsNotVerified] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const router = useRouter();
  const dispatch = useDispatch();

  const checkUserStatus = async () => {
    setIsLoading(true);
    const data = await verifyUserStatus();

    if (!data.isVerified) {
      setIsLoading(false);
      setIsNotVerified(true);
    } else {
      setIsLoading(false);
      router.replace("/");
      dispatch(userActions.setUserData({ ...data }));
    }
  };

  return (
    <Fragment>
      <p className="modalHeading">Verify</p>
      <div className={styles.container}>
        <div className={styles.verify}>
          <p className={styles.heading2}>Welcome {name}</p>

          <div className={styles.verification}>
            <p>Please check your email for verification link.</p>
            <button>
              <a
                href="https://mail.google.com/"
                target="_blank"
                rel="noreferrer"
              >
                Open Mail Box
              </a>
              <HiOutlineMail className="icon" />
            </button>
          </div>

          <div className={styles.verified}>
            <p>If your email is sucessfully verified, Click Next.</p>
            {isLoading ? (
              <Spinner />
            ) : (
              <button onClick={checkUserStatus}>
                Next
                <MdNavigateNext className="icon" />
              </button>
            )}
          </div>

          {isNotVerified && (
            <p className={styles.errText}>
              You account is not verified yet, please check your mailbox.
              <br /> An activation link has been send to your mail address{" "}
              {email}
            </p>
          )}
        </div>
        <div>
          <p>
            If you think, you have entered wrong email, you can Logout from
            here.
          </p>
          <LogoutButton />
        </div>
      </div>
    </Fragment>
  );
};

export default VerifyUser;
