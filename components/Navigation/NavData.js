import { AiFillSetting } from "react-icons/ai";
import { BiTask } from "react-icons/bi";
import { FaBook, FaCalendarAlt } from "react-icons/fa";
import { HiHome } from "react-icons/hi";
import { IoNewspaperSharp } from "react-icons/io5";
import { ImBullhorn } from "react-icons/im";

export const MENU_DATA = [
  {
    title: "Notice",
    link: "/notice",
    icon: <IoNewspaperSharp />,
  },
  {
    title: "Assignments",
    link: "/assignments",
    icon: <BiTask />,
  },
  {
    title: "Time Table",
    link: "/time-table",
    icon: <FaCalendarAlt />,
  },
  {
    title: "Home",
    link: "/",
    icon: <HiHome />,
  },
  {
    title: "Jobs",
    link: "/jobs",
    icon: <ImBullhorn />,
  },
  {
    title: "Study Material",
    link: "/study-material",
    icon: <FaBook />,
  },
  {
    title: "More",
    link: "/more",
    icon: <AiFillSetting />,
  },
];
