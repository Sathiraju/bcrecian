import styles from "./Nav.module.scss";
import { useDispatch, useSelector } from "react-redux";
import { themeActions } from "../../store/theme";
import { FiMoon, FiSun } from "react-icons/fi";
import { useEffect } from "react";
import { FaUser } from "react-icons/fa";
import Link from "next/link";
import NavIcon from "./NavIcon";

import { AiOutlineUser } from "react-icons/ai";
import Navigation from "./Navigation";
import Searchbar from "../SearchBar/Searchbar";
import NotifyIcon from "../Notification/Index";

const Navigationbar = () => {
  const dispatch = useDispatch();
  const theme = useSelector((state) => state.theme.color);
  const { isAddedDetails } = useSelector((state) => state.user);

  useEffect(() => {
    const prevTheme = localStorage.getItem("theme");
    if (prevTheme) {
      dispatch(themeActions.setTheme(prevTheme));
    }
  }, [dispatch]);

  const handleBtn = () => {
    dispatch(themeActions.toggleTheme());
  };

  return (
    <div className={styles.container}>
      <header className={styles.header}>
        <nav className={styles.nav}>
          <Link href="/" passHref>
            <div className={styles.logo}>
              <h3 className={styles.logoText}>CS</h3>
            </div>
          </Link>
          {isAddedDetails && <Searchbar />}
          <div className={styles.menu}>
            <NavIcon Icon={AiOutlineUser} ActiveIcon={FaUser} />
            {isAddedDetails && <NotifyIcon />}
            <div
              className={styles.tgBtn}
              onClick={handleBtn}
              title={`${
                theme === "light" ? "Turn off the Light" : "Turn on the light"
              }`}
            >
              {theme === "light" ? <FiMoon /> : <FiSun />}
            </div>
          </div>
        </nav>
      </header>
      <Navigation />
    </div>
  );
};

export default Navigationbar;
