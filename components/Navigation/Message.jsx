const Message = () => {
  return (
    <>
      <p className="modalHeading">Profile</p>
      <p style={{ marginTop: "3rem" }}>
        Your Profile details will be shown here.
      </p>
      <h1 style={{ marginTop: "2rem" }}>
        To Get Started, You need To Login / Register.
      </h1>
    </>
  );
};

export default Message;
