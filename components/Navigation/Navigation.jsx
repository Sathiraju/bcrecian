import Link from "next/link";
import classes from "./Navigation.module.scss";
import Tippy from "@tippyjs/react";
import "tippy.js/dist/tippy.css";
import { forwardRef } from "react";

import { MENU_DATA } from "./NavData";
import { useRouter } from "next/router";

// eslint-disable-next-line react/display-name
const CustomLink = forwardRef(({ icon, pathname, link, switchRoute, router }, ref) => {
  // console.log(pathname, link);
  return (
    <div
      onClick={() => switchRoute(link)}
      ref={ref}
      className={`${classes.icon} ${pathname === link ? classes.active : ""}`}
    >
      {icon}
    </div>
  );
});

const ToolTip = ({ title }) => {
  return <span className={classes.tooltip}>{title}</span>;
};

const Navigation = () => {
  const router = useRouter();

  const switchRoute = (link) => {
    router.push(link);
  };

  return (
    <header className={classes.bottomHeader}>
      <nav className={classes.list}>
        {MENU_DATA.map((icon) => (
          <Tippy
            key={icon.link}
            content={<ToolTip title={icon.title} />}
            arrow={false}
            placement="bottom"
            className={classes.newTippy}
            delay={200}
          >
            {
              <CustomLink
                icon={icon.icon}
                link={icon.link}
                pathname={router.asPath}
                switchRoute={switchRoute}
              />
            }
          </Tippy>
        ))}
      </nav>
    </header>
  );
};

export default Navigation;
