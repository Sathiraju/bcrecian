import Animation from "./Animation";

import styles from "./Animation.module.scss";

const Fallback = () => {
  return (
    <div className={styles.center}>
      <Animation />
      <div className={styles.container}>
        <p className={styles.errText1}>ERROR</p>
        <p className={styles.errText2}>
          Check your internet connection or WIFI signals 📶
        </p>
        <p className={styles.errText2}>Then try again.</p>
      </div>
    </div>
  );
};

export default Fallback;
