import styles from "./Download.module.scss";

import React from "react";

const Download = () => {
  return (
    <div>
      <p className="modalHeading">Instructions</p>
      <div className={styles.details}>
        <p>To receiving notifications, please allow notifications when popup arrives.</p>
        <p>To download our app, click on the popup and then click on install.</p>
        <div className="extras">
          <p>Facing any problems?</p>
          <p>Click Here</p>
        </div>
      </div>
    </div>
  );
};

export default Download;
