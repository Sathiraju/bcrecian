## Working Directory

### Welcome to College Space.

#### Our Developers
Raj - Full Stack

Ananda - Backend

Tirtharaj - Frontend

Shreya - Database

## Getting Started

First clone the project : 

```bash
git clone https://gitlab.com/Raj-kar/bcrecian.git
cd bcrecian
npm install
```

##### Then make sure, to create your own branch. 
##### Never edit or modify in master branch.
&nbsp;

## How to Create own branch ?

```bash
git checkout -b <your branch name>
```

Now, You can edit or run the development server:

For run the development server :

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.js`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.